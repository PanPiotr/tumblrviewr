package com.example.tumblrviewr;

import com.example.tumblrviewr.data.RepositoryComponent;

import dagger.Component;

/**
 * Created by Piotr on 07.03.2017.
 */

@Component(dependencies = RepositoryComponent.class)
public interface ApiUnitTestComponent {
    void inject(ApiUnitTest test);
}
