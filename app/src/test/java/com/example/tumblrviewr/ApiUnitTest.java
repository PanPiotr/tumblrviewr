package com.example.tumblrviewr;

import com.example.tumblrviewr.data.PostsRepository;
import com.example.tumblrviewr.data.api.TumblrApiPostDataSource;
import com.example.tumblrviewr.data.model.Response;
import com.example.tumblrviewr.data.model.posts.Post;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import rx.Observable;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by pkuszewski on 16.02.2017.
 */

public class ApiUnitTest {

    private PostsRepository repository;

    @Before
    public void setup() {
        repository = new PostsRepository(new TumblrApiPostDataSource());
    }

    @Test
    public void testConnection() {
        Observable<List<Post>> observable = repository.getPosts("test", 0, 1)
                                                      .map(Response::getPosts);
        assertNotNull("Observable from API is null", observable);
        List<Post> posts = observable
                .toBlocking()
                .single();
        assertNotNull("Returned posts are null", posts);
        assertEquals("Post count not as expected, should be 1", 1, posts.size());
    }

    @Test
    public void testOffsetFetch() {
        Observable<List<Post>> observable = repository.getPosts("test", 0, 1)
                                                      .map(Response::getPosts);
        assertNotNull("Observable from API is null", observable);
        List<Post> posts = observable.toBlocking().single();
        assertNotNull("Returned posts are null", posts);
        assertEquals("Post count not as expected, should be 1", 1, posts.size());
        Post post1 = posts.get(0);
        observable = repository.getPosts("test", 1, 1)
                               .map(Response::getPosts);
        assertNotNull("Observable from API is null", observable);
        posts = observable.toBlocking().single();
        assertNotNull("Returned posts are null", posts);
        assertEquals("Post count not as expected, should be 1", 1, posts.size());
        Post post2 = posts.get(0);
        assertNotEquals("Posts should be different", post1.getId(), post2.getId());
    }

    @Test
    public void downloadTen() {
        Observable<List<Post>> observable = repository.getPosts("test", 0, 10)
                                                      .map(Response::getPosts);
        assertNotNull("Observable from API is null", observable);
        List<Post> posts = observable.toBlocking().single();
        assertNotNull("Returned posts are null", posts);
        assertEquals("Post count not as expected, should be 10", 10, posts.size());
    }

}
