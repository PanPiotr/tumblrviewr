package com.example.tumblrviewr;

import android.app.Application;

import com.example.tumblrviewr.data.DaggerRepositoryComponent;
import com.example.tumblrviewr.data.RepositoryComponent;

import lombok.Getter;

/**
 * Created by Piotr on 07.03.2017.
 */

public class TumblrViewrApplication extends Application {

    @Getter
    RepositoryComponent postsRepositoryComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        postsRepositoryComponent = DaggerRepositoryComponent.builder().build();
    }
}
