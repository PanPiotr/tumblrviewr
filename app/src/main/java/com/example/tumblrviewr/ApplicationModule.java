package com.example.tumblrviewr;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Piotr on 07.03.2017.
 */
@Module
public final class ApplicationModule {

    private final Context mContext;

    ApplicationModule(Context context) {
        mContext = context;
    }

    @Provides
    Context provideContext() {
        return mContext;
    }


}
