package com.example.tumblrviewr.list;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.tumblrviewr.R;
import com.example.tumblrviewr.data.model.posts.Post;
import com.example.tumblrviewr.data.model.posts.PostType;
import com.example.tumblrviewr.list.items.PostItemViewHolder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by pkuszewski on 16.02.2017.
 */

public class PostListAdapter extends RecyclerView.Adapter<PostItemViewHolder> {

    @Setter
    @Getter
    private List<Post> postList = new ArrayList<>();

    private Map<Integer, Integer> viewMapping = new HashMap<>();

    public PostListAdapter() {
        prepareMapping();
    }

    @Override
    public PostItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PostItemViewHolder holder;
        Integer layout = viewMapping.get(viewType);
        if (layout == null) {
            layout = R.layout.simple_post_list_item;
        }
        View view = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        holder = new PostItemViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(PostItemViewHolder holder, int position) {
        Post post = getPostList().get(position);
        holder.root.setPost(post);
    }

    @Override
    public int getItemCount() {
        return postList.size();
    }

    @Override
    public int getItemViewType(int position) {
        Post post = getPostList().get(position);
        return post.getType().getOrder();
    }

    private void prepareMapping() {
        viewMapping.put(PostType.REGULAR.getOrder(), R.layout.regular_post_list_item);
        viewMapping.put(PostType.PHOTO.getOrder(), R.layout.photo_post_list_item);
        viewMapping.put(PostType.QUOTE.getOrder(), R.layout.quote_post_list_item);
        viewMapping.put(PostType.LINK.getOrder(), R.layout.link_post_list_item);
    }
}
