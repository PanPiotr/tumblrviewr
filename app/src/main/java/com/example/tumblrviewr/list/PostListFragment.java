package com.example.tumblrviewr.list;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.tumblrviewr.R;
import com.example.tumblrviewr.data.model.posts.Post;
import com.jakewharton.rxbinding.widget.RxTextView;
import com.jakewharton.rxbinding.widget.TextViewAfterTextChangeEvent;

import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by pkuszewski on 16.02.2017.
 */

public class PostListFragment extends Fragment implements PostListContract.View {

    @BindView(R.id.username_picker)
    EditText usernamePicker;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.post_list_recycler)
    RecyclerView postListRecycler;
    @BindView(R.id.refresh_layout)
    SwipeRefreshLayout refreshLayout;

    private PostListContract.Presenter presenter;
    private PostListAdapter adapter;
    private LinearLayoutManager layoutManager;
    private Subscription subscription;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_post_list, container, false);
        ButterKnife.bind(this, view);
        setupViews();
        return view;
    }

    @Override
    public void onDestroyView() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
        super.onDestroyView();
    }

    private void setupViews() {
        refreshLayout.setOnRefreshListener(() -> presenter.changeUser(getUsername()));
        adapter = new PostListAdapter();
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        postListRecycler.setLayoutManager(layoutManager);
        postListRecycler.setAdapter(adapter);
        postListRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (adapter.getItemCount() > 0 &&
                        adapter.getItemCount() - layoutManager
                                .findLastCompletelyVisibleItemPosition() < 5
                        && !refreshLayout.isRefreshing()
                        && dy > 0) {
                    presenter.getPosts(adapter.getItemCount());
                }
            }
        });

        subscription = RxTextView.afterTextChangeEvents(usernamePicker)
                                 .debounce(2, TimeUnit.SECONDS)
                                 .filter(textViewAfterTextChangeEvent -> textViewAfterTextChangeEvent != null)
                                 .map(TextViewAfterTextChangeEvent::editable)
                                 .map(CharSequence::toString)
                                 .filter(text -> !TextUtils.isEmpty(text))
                                 .observeOn(AndroidSchedulers.mainThread())
                                 .subscribe(text -> presenter.changeUser(text));

    }

    @Override
    public void clearList() {
        adapter.getPostList().clear();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void addPosts(List<Post> newPosts) {
        int oldSize = adapter.getItemCount();
        adapter.getPostList().addAll(newPosts);
        adapter.notifyItemRangeInserted(oldSize, newPosts.size());
    }

    @Override
    public void showLoading() {
        refreshLayout.setRefreshing(true);
    }

    @Override
    public void hideLoading() {
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void showError(@StringRes int errorMessage) {
        if (getActivity() == null) {
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setPositiveButton(R.string.ok, null);
        builder.setTitle(R.string.error);
        builder.setMessage(errorMessage);
        builder.show();
    }

    private String getUsername() {
        return "test";
    }

    @Override
    public void setPresenter(PostListContract.Presenter presenter) {
        this.presenter = presenter;
    }
}
