package com.example.tumblrviewr.list.items;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;

import com.example.tumblrviewr.R;
import com.example.tumblrviewr.data.model.posts.QuotePost;

import butterknife.BindView;

/**
 * Created by pkuszewski on 17.02.2017.
 */
public class QuotePostItemView extends PostItemView<QuotePost> {

    @BindView(R.id.text)
    TextView text;

    @BindView(R.id.source)
    TextView source;

    public QuotePostItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setPost(QuotePost quotePost) {
        if (!TextUtils.isEmpty(quotePost.getText())) {
            text.setVisibility(VISIBLE);
            text.setText(quotePost.getText());
        } else {
            text.setVisibility(GONE);
        }
        if (!TextUtils.isEmpty(quotePost.getSource())) {
            source.setVisibility(VISIBLE);
            source.setText(quotePost.getSource());
        } else {
            source.setVisibility(GONE);
        }
    }
}
