package com.example.tumblrviewr.list.items;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tumblrviewr.R;
import com.example.tumblrviewr.data.model.posts.LinkPost;

import butterknife.BindView;

/**
 * Created by pkuszewski on 20.02.2017.
 */

public class LinkPostItemView extends PostItemView<LinkPost> {

    @BindView(R.id.text)
    TextView text;

    @BindView(R.id.description)
    TextView description;

    public LinkPostItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setPost(LinkPost linkPost) {
        if (TextUtils.isEmpty(linkPost.getText())) {
            linkPost.setText(getContext().getString(R.string.link));
        }
        if (TextUtils.isEmpty(linkPost.getUrl())) {
            linkPost.setUrl("http://tumblr.com");
        }
        text.setText(linkPost.getText());
        String url = linkPost.getUrl();
        if (!url.startsWith("http://") && !url.startsWith("https://")) {
            url = "http://" + url;
        }
        final String finalUrl = url;
        text.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(finalUrl));
                try {
                    getContext().startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(getContext(), R.string.no_app_error, Toast.LENGTH_SHORT).show();
                }

            }
        });

        if (!TextUtils.isEmpty(linkPost.getDescription())) {
            description.setVisibility(VISIBLE);
            description.setText(linkPost.getDescription());
        } else {
            description.setVisibility(GONE);
        }
    }
}
