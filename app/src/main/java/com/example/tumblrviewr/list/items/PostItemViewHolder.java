package com.example.tumblrviewr.list.items;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by pkuszewski on 17.02.2017.
 */

public class PostItemViewHolder extends RecyclerView.ViewHolder {

    public PostItemView root;

    public PostItemViewHolder(View itemView) {
        super(itemView);
        if (itemView instanceof PostItemView) {
            root = (PostItemView) itemView;
        } else {
            throw new IllegalArgumentException("Root view must be subclass of PostItemView");
        }
    }
}
