package com.example.tumblrviewr.list;

import com.example.tumblrviewr.data.RepositoryComponent;
import com.example.tumblrviewr.util.FragmentScoped;

import dagger.Component;

/**
 * Created by Piotr on 07.03.2017.
 */

@FragmentScoped
@Component(dependencies = RepositoryComponent.class, modules = PostListPresenterModule.class)
public interface PostListComponent {

    void inject(PostListActivity activity);

}
