package com.example.tumblrviewr.list.items;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.example.tumblrviewr.R;
import com.example.tumblrviewr.data.model.posts.Post;

import butterknife.BindView;

/**
 * Created by pkuszewski on 17.02.2017.
 */

public class SimplePostItemView extends PostItemView {

    @BindView(R.id.id)
    TextView id;

    public SimplePostItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setPost(Post post) {
        id.setText(post.getId());
    }
}
