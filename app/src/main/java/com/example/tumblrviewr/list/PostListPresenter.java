package com.example.tumblrviewr.list;

import com.example.tumblrviewr.R;
import com.example.tumblrviewr.data.PostsDataSource;
import com.example.tumblrviewr.data.model.Response;
import com.example.tumblrviewr.data.model.posts.PostType;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by pkuszewski on 16.02.2017.
 */

public class PostListPresenter implements PostListContract.Presenter {

    private static final int DEFAULT_FETCH = 10;

    private PostListContract.View view;

    private PostsDataSource repository;
    private String currentUsername = "";

    private int totalPostCount = 0;

    private List<PostType> supportedTypes = Arrays.asList(
            PostType.QUOTE,
            PostType.PHOTO,
            PostType.REGULAR,
            PostType.LINK);

    @Inject
    public PostListPresenter(PostListContract.View view, PostsDataSource repository) {
        this.view = view;
        this.repository = repository;
    }

    @Inject
    public void setupListeners() {
        this.view.setPresenter(this);
    }

    @Override
    public void changeUser(String username) {
        view.clearList();
        view.showLoading();
        currentUsername = username;
        addPostsToView(repository.getPosts(username, 0, DEFAULT_FETCH));

    }

    @Override
    public void getPosts(int offset) {
        view.showLoading();
        int fetch = 0;
        if (offset >= totalPostCount) {
            view.hideLoading();
            return;
        }
        if (offset + DEFAULT_FETCH > totalPostCount) {
            fetch = totalPostCount - offset;
        } else {
            fetch = DEFAULT_FETCH;
        }
        addPostsToView(repository.getPosts(currentUsername, offset, fetch));
    }

    private void addPostsToView(Observable<Response> observable) {
        observable.subscribeOn(Schedulers.io())
                  .observeOn(AndroidSchedulers.mainThread())
                  .map(response -> {
                      totalPostCount = response.getTotalCount();
                      return response.getPosts();
                  })
                  .flatMapIterable(posts -> posts)
                  .filter(post -> supportedTypes.contains(post.getType()))
                  .toList()
                  .subscribe(posts -> {
                      view.addPosts(posts);
                      view.hideLoading();
                  }, throwable -> {
                      throwable.printStackTrace();
                      view.showError(R.string.network_error);
                      view.hideLoading();
                  });
    }

    @Override
    public void start() {

    }
}
