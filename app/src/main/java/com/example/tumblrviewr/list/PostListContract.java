package com.example.tumblrviewr.list;

import android.support.annotation.StringRes;

import com.example.tumblrviewr.BasePresenter;
import com.example.tumblrviewr.BaseView;
import com.example.tumblrviewr.data.model.posts.Post;

import java.util.List;

/**
 * Created by pkuszewski on 16.02.2017.
 */

public interface PostListContract {

    interface View extends BaseView<Presenter> {

        void clearList();

        void addPosts(List<Post> newPosts);

        void showLoading();

        void hideLoading();

        void showError(@StringRes int errorMessage);

    }

    interface Presenter extends BasePresenter {

        void changeUser(String username);

        void getPosts(int offset);
    }

}
