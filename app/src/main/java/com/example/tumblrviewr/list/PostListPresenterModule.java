package com.example.tumblrviewr.list;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Piotr on 07.03.2017.
 */

@Module
public class PostListPresenterModule {

    private final PostListContract.View view;

    PostListPresenterModule(PostListContract.View view) {
        this.view = view;
    }

    @Provides
    PostListContract.View provideContractView() {
        return view;
    }
}
