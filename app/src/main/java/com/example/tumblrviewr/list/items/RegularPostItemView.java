package com.example.tumblrviewr.list.items;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;

import com.example.tumblrviewr.R;
import com.example.tumblrviewr.data.model.posts.RegularPost;

import butterknife.BindView;

/**
 * Created by pkuszewski on 17.02.2017.
 */

public class RegularPostItemView extends PostItemView<RegularPost> {

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.body)
    TextView body;

    public RegularPostItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    public void setPost(RegularPost regularPost) {
        if (!TextUtils.isEmpty(regularPost.getTitle())) {
            title.setVisibility(VISIBLE);
            title.setText(regularPost.getTitle());
        } else {
            title.setVisibility(GONE);
        }

        if (!TextUtils.isEmpty(regularPost.getBody())) {
            body.setVisibility(VISIBLE);
            body.setText(regularPost.getBody());
        } else {
            body.setVisibility(GONE);
        }

    }
}
