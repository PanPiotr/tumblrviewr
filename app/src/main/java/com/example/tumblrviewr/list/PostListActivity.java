package com.example.tumblrviewr.list;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.tumblrviewr.R;
import com.example.tumblrviewr.TumblrViewrApplication;

import javax.inject.Inject;

public class PostListActivity extends AppCompatActivity {


    @Inject
    PostListPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_list);
        prepareAndShowPostList();
    }

    private void prepareAndShowPostList() {

        PostListFragment view = new PostListFragment();
        DaggerPostListComponent
                .builder()
                .repositoryComponent(
                        ((TumblrViewrApplication) getApplication()).getPostsRepositoryComponent())
                .postListPresenterModule(new PostListPresenterModule(view))
                .build()
                .inject(this);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, view)
                                   .commit();


    }
}
