package com.example.tumblrviewr.list.items;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.example.tumblrviewr.data.model.posts.Post;

import butterknife.ButterKnife;

/**
 * Created by pkuszewski on 17.02.2017.
 */

public abstract class PostItemView<T extends Post> extends LinearLayout {

    public PostItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this, this);
    }

    public abstract void setPost(T post);

}
