package com.example.tumblrviewr.list.items;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tumblrviewr.R;
import com.example.tumblrviewr.data.model.posts.PhotoPost;
import com.squareup.picasso.Picasso;

import butterknife.BindView;

/**
 * Created by pkuszewski on 17.02.2017.
 */

public class PhotoPostItemView extends PostItemView<PhotoPost> {

    private static final String TAG = PhotoPostItemView.class.getSimpleName();
    @BindView(R.id.image)
    ImageView image;

    @BindView(R.id.photo_caption)
    TextView caption;

    @BindView(R.id.photo_caption_background)
    View background;

    public PhotoPostItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setPost(PhotoPost photoPost) {
        if (!TextUtils.isEmpty(photoPost.getCaption())) {
            background.setVisibility(VISIBLE);
            caption.setVisibility(VISIBLE);
            caption.setText(photoPost.getCaption());
        } else {
            background.setVisibility(GONE);
            caption.setVisibility(GONE);
        }
        Log.d(TAG, "setPost: loading url: " + photoPost.getPhotoUrlSmall());
        Picasso.with(getContext())
               .load(photoPost.getPhotoUrlSmall())
               .into(image);
    }
}
