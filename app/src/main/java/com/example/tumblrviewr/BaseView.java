package com.example.tumblrviewr;

/**
 * Created by Piotr on 07.03.2017.
 */

public interface BaseView<T extends BasePresenter> {
    void setPresenter(T presenter);
}
