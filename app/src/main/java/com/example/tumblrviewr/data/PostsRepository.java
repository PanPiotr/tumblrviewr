package com.example.tumblrviewr.data;

import com.example.tumblrviewr.data.model.Response;

import javax.inject.Inject;

import dagger.Module;
import rx.Observable;

/**
 * Created by Piotr on 07.03.2017.
 */
@Module()
public class PostsRepository implements PostsDataSource {

    private PostsDataSource remoteDataSource;

    @Inject
    public PostsRepository(PostsDataSource remoteDataSource) {
        this.remoteDataSource = remoteDataSource;
    }

    @Override
    public Observable<Response> getPosts(String username, int offset, int count) {
        return remoteDataSource.getPosts(username, offset, count);
    }
}
