package com.example.tumblrviewr.data.api;

import com.example.tumblrviewr.data.PostsDataSource;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Piotr on 07.03.2017.
 */
@Module
public class TumblrApiPostDataSourceModule {

    @Provides
    @Singleton
    PostsDataSource getPostDataSource() {
        return new TumblrApiPostDataSource();
    }
}
