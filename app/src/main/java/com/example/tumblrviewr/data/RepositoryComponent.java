package com.example.tumblrviewr.data;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Piotr on 07.03.2017.
 */
@Singleton
@Component(modules = PostRepositoryModule.class)
public interface RepositoryComponent {

    PostsDataSource getPostsRepository();

}
