package com.example.tumblrviewr.data;

import com.example.tumblrviewr.data.model.Response;

import rx.Observable;

/**
 * Created by pkuszewski on 16.02.2017.
 */

public interface PostsDataSource {

    Observable<Response> getPosts(String username, int offset, int count);

}
