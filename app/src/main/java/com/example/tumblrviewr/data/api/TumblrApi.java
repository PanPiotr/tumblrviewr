package com.example.tumblrviewr.data.api;

import com.example.tumblrviewr.data.model.Response;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by pkuszewski on 16.02.2017.
 */

public interface TumblrApi {

    @GET("api/read/json")
    Observable<Response> getPosts(
            @Query("debug") int isDebug,
            @Query("type") String type,
            @Query("start") int offset,
            @Query("num") int fetch,
            @Query("filter") String filter
    );

}
