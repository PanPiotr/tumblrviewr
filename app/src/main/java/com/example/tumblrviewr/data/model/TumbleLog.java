package com.example.tumblrviewr.data.model;

import java.util.List;

import lombok.Data;

/**
 * Created by pkuszewski on 16.02.2017.
 */
@Data
public class TumbleLog {

    private String title;

    private String description;

    private String name;

    private String timezone;

    private boolean cname;

    private List<String> feeds;
}
