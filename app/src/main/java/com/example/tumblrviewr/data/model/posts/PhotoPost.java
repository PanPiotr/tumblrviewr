package com.example.tumblrviewr.data.model.posts;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

/**
 * Created by pkuszewski on 16.02.2017.
 */
@Data
public class PhotoPost extends Post {

    @SerializedName("photo-caption")
    private String caption;

    @SerializedName("photo-url-1280")
    private String photoUrlBig;

    @SerializedName("photo-url-400")
    private String photoUrlSmall;

    @Override
    public PostType getType() {
        return PostType.PHOTO;
    }
}
