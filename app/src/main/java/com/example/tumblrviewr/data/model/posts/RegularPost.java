package com.example.tumblrviewr.data.model.posts;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

/**
 * Created by pkuszewski on 16.02.2017.
 */
@Data
public class RegularPost extends Post {

    @SerializedName("regular-title")
    private String title;

    @SerializedName("regular-body")
    private String body;

    @Override
    public PostType getType() {
        return PostType.REGULAR;
    }
}
