package com.example.tumblrviewr.data.model.posts;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

/**
 * Created by pkuszewski on 16.02.2017.
 */
@Data
public class LinkPost extends Post {
    @Override
    public PostType getType() {
        return PostType.LINK;
    }

    @SerializedName("link-text")
    private String text;

    @SerializedName("link-url")
    private String url;

    @SerializedName("link-description")
    private String description;
}
