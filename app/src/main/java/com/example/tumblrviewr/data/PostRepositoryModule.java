package com.example.tumblrviewr.data;

import com.example.tumblrviewr.data.api.TumblrApiPostDataSourceModule;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Piotr on 07.03.2017.
 */
@Module(includes = TumblrApiPostDataSourceModule.class)
public class PostRepositoryModule {

    @Provides
    PostsRepository providePostsRepository(PostsDataSource postsDataSource) {
        return new PostsRepository(postsDataSource);
    }

}
