package com.example.tumblrviewr.data.model;

import com.example.tumblrviewr.data.model.posts.Post;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Data;

/**
 * Created by pkuszewski on 16.02.2017.
 */
@Data
public class Response {

    private TumbleLog tumblelog;

    @SerializedName("posts-start")
    private Integer offset;

    @SerializedName("posts-total")
    private Integer totalCount;

    private List<Post> posts;
}
