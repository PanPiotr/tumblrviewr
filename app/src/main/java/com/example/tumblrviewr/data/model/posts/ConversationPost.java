package com.example.tumblrviewr.data.model.posts;

/**
 * Created by pkuszewski on 17.02.2017.
 */

public class ConversationPost extends Post {
    @Override
    public PostType getType() {
        return PostType.CONVERSATION;
    }
}
