package com.example.tumblrviewr.data.model.posts;

import com.google.gson.annotations.SerializedName;

/**
 * Created by pkuszewski on 16.02.2017.
 */
public enum PostType {


    @SerializedName("regular")
    REGULAR(1),

    @SerializedName("link")
    LINK(4),

    @SerializedName("video")
    VIDEO(9),

    @SerializedName("audio")
    AUDIO(99),

    @SerializedName("chat")
    CHAT(999),

    @SerializedName("quote")
    QUOTE(3),

    @SerializedName("photo")
    PHOTO(2),

    @SerializedName("conversation")
    CONVERSATION(8),

    @SerializedName("answer")
    ANSWER(10);

    private int order;

    PostType(int order) {
        this.order = order;
    }

    public int getOrder() {
        return order;
    }
}
