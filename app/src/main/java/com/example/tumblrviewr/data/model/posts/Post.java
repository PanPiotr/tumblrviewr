package com.example.tumblrviewr.data.model.posts;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

import lombok.Data;

/**
 * Created by pkuszewski on 16.02.2017.
 */
@Data
public abstract class Post {

    private String id;

    private String url;

    @SerializedName("url-with-slug")
    private String urlWithSlug;

    @SerializedName("date-gmt")
    private Date dateGmt;

    private List<String> tags;

    public abstract PostType getType();

}
