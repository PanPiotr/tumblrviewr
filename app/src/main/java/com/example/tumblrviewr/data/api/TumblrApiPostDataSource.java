package com.example.tumblrviewr.data.api;

import android.support.annotation.NonNull;

import com.example.tumblrviewr.data.PostsDataSource;
import com.example.tumblrviewr.data.model.Response;
import com.example.tumblrviewr.data.model.posts.AnswerPost;
import com.example.tumblrviewr.data.model.posts.AudioPost;
import com.example.tumblrviewr.data.model.posts.ChatPost;
import com.example.tumblrviewr.data.model.posts.ConversationPost;
import com.example.tumblrviewr.data.model.posts.LinkPost;
import com.example.tumblrviewr.data.model.posts.PhotoPost;
import com.example.tumblrviewr.data.model.posts.Post;
import com.example.tumblrviewr.data.model.posts.QuotePost;
import com.example.tumblrviewr.data.model.posts.RegularPost;
import com.example.tumblrviewr.data.model.posts.VideoPost;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.typeadapters.RuntimeTypeAdapterFactory;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;

/**
 * Created by pkuszewski on 16.02.2017.
 */
@Singleton
public class TumblrApiPostDataSource implements PostsDataSource {

    private String currentUsername = "";

    private TumblrApi api;

    @Override
    public Observable<Response> getPosts(String username, int offset, int fetch) {
        if (api == null || !username.equals(currentUsername)) {
            api = createApi(username);
            currentUsername = username;
        }
        return api.getPosts(1, null, offset, fetch, "text");
    }

    @NonNull
    private TumblrApi createApi(String username) {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(loggingInterceptor).build();
        RuntimeTypeAdapterFactory<Post> factory = RuntimeTypeAdapterFactory
                .of(Post.class, "type")
                .registerSubtype(RegularPost.class, "regular")
                .registerSubtype(PhotoPost.class, "photo")
                .registerSubtype(LinkPost.class, "link")
                .registerSubtype(VideoPost.class, "video")
                .registerSubtype(AudioPost.class, "audio")
                .registerSubtype(ChatPost.class, "chat")
                .registerSubtype(QuotePost.class, "quote")
                .registerSubtype(ConversationPost.class, "conversation")
                .registerSubtype(AnswerPost.class, "answer");


        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(factory)
                .setDateFormat("yyyy-MM-dd HH:mm:ss ZZZ")
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(getBaseUrl(username))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

        TumblrApi api = retrofit.create(TumblrApi.class);
        return api;
    }

    private static String getBaseUrl(String username) {
        return String.format("http://%s.tumblr.com/", username);
    }
}
