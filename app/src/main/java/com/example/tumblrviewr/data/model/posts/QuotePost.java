package com.example.tumblrviewr.data.model.posts;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

/**
 * Created by pkuszewski on 16.02.2017.
 */
@Data
public class QuotePost extends Post {
    @Override
    public PostType getType() {
        return PostType.QUOTE;
    }

    @SerializedName("quote-text")
    private String text;

    @SerializedName("quote-source")
    private String source;

}
